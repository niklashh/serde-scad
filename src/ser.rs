//! Serialize a Rust data structure into Scad.

use serde::ser::{self, Serialize};

use crate::error::{Error, Result};
use crate::utils::MapKeySerializer;

/// A structure for serializing Rust values into Scad.
pub struct Serializer {
    output: String,
}

/// Generate Scad from the serializable value.
///
/// # Errors
///
/// Serialization can fail in the following cases:
/// - `T`'s implementation of `Serialize` decides to fail
/// - `T` contains a map with keys that don't implement Display
/// - `T` contains a map as a value
pub fn to_string<T>(value: &T) -> Result<String>
where
    T: Serialize,
{
    let mut serializer = Serializer {
        output: String::new(),
    };
    value.serialize(&mut serializer)?;
    Ok(serializer.output)
}

impl<'a> ser::Serializer for &'a mut Serializer {
    type Ok = ();
    type Error = Error;

    type SerializeSeq = Self;
    type SerializeTuple = Self;
    type SerializeTupleStruct = Self;
    type SerializeTupleVariant = Self;
    type SerializeMap = Self;
    type SerializeStruct = Self;
    type SerializeStructVariant = Self;

    fn serialize_bool(self, v: bool) -> Result<()> {
        self.output += if v { "true" } else { "false" };
        Ok(())
    }

    fn serialize_i8(self, v: i8) -> Result<()> {
        self.serialize_i64(i64::from(v))
    }

    fn serialize_i16(self, v: i16) -> Result<()> {
        self.serialize_i64(i64::from(v))
    }

    fn serialize_i32(self, v: i32) -> Result<()> {
        self.serialize_i64(i64::from(v))
    }

    fn serialize_i64(self, v: i64) -> Result<()> {
        // TODO: itoa
        self.output += &v.to_string();
        Ok(())
    }

    fn serialize_u8(self, v: u8) -> Result<()> {
        self.serialize_u64(u64::from(v))
    }

    fn serialize_u16(self, v: u16) -> Result<()> {
        self.serialize_u64(u64::from(v))
    }

    fn serialize_u32(self, v: u32) -> Result<()> {
        self.serialize_u64(u64::from(v))
    }

    fn serialize_u64(self, v: u64) -> Result<()> {
        self.output += &v.to_string();
        Ok(())
    }

    fn serialize_f32(self, v: f32) -> Result<()> {
        self.serialize_f64(f64::from(v))
    }

    fn serialize_f64(self, v: f64) -> Result<()> {
        self.output += &v.to_string();
        Ok(())
    }

    /// Serialize a char as a single-character string.
    fn serialize_char(self, v: char) -> Result<()> {
        self.serialize_str(&v.to_string())
    }

    /// FIXME: This only works for strings that don't require escape
    /// sequences. For example it would emit invalid scad if the
    /// input string contains a '"' character.
    fn serialize_str(self, v: &str) -> Result<()> {
        self.output += "\"";
        self.output += v;
        self.output += "\"";
        Ok(())
    }

    /// Serialize a byte array as an array of bytes.
    fn serialize_bytes(self, v: &[u8]) -> Result<()> {
        use serde::ser::SerializeSeq;
        let mut seq = self.serialize_seq(Some(v.len()))?;
        for byte in v {
            seq.serialize_element(byte)?;
        }
        seq.end()
    }

    /// An absent optional is represented as nothing in scad.
    fn serialize_none(self) -> Result<()> {
        // TODO: is this a hack?
        let truncate_len = self
            .output
            .rfind(',') // the comma should be removed
            // but `(` should not be removed
            .or_else(|| self.output.rfind('(').map(|index| index + 1))
            .unwrap_or_else(|| {
                panic!(
                    "expected `{}` to contain `,` or `(` to truncate the string to.",
                    &self.output,
                )
            });
        self.output.truncate(truncate_len);
        Ok(())
    }

    /// A present optional is represented as just the contained value.
    fn serialize_some<T>(self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(self)
    }

    /// In Serde, unit means an anonymous value containing no data.
    /// Map this to `undef` in scad.
    fn serialize_unit(self) -> Result<()> {
        self.output += "undef";
        Ok(())
    }

    /// Unit struct means a named value containing no data.
    /// However in scad we can have objects which take no input,
    /// therefore map unit structs to objects of the same name:
    ///
    /// ```rust
    /// # use serde_scad::ser::to_string;
    /// # use serde::Serialize;
    /// #[derive(Serialize)]
    /// struct Cube;
    ///
    /// assert_eq!(to_string(&Cube).unwrap(), "Cube()");
    /// ```
    fn serialize_unit_struct(self, name: &'static str) -> Result<()> {
        use ser::SerializeTupleStruct;
        let ts = self.serialize_tuple_struct(name, 0)?;
        ts.end()
    }

    /// A unit variant is serialized into a unit struct
    fn serialize_unit_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
    ) -> Result<()> {
        Err(Error::Message(
            "Unit variants are disabled in favor of empty tuple variants.".to_string(),
        ))
    }

    /// Newtype structs are serialized like tuple structs.
    fn serialize_newtype_struct<T>(self, name: &'static str, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        use ser::SerializeTupleStruct;
        let mut ts = self.serialize_tuple_struct(name, 1)?;
        ts.serialize_field(value)?;
        ts.end()
    }

    /// Newtype variants are serialized like tuple structs.
    fn serialize_newtype_variant<T>(
        self,
        name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
        value: &T,
    ) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        use ser::SerializeTupleStruct;
        let mut ts = self.serialize_tuple_struct(name, 1)?;
        ts.serialize_field(value)?;
        ts.end()
    }

    /// Now we get to the serialization of compound types.
    ///
    /// The start of the sequence, each value, and the end are three separate
    /// method calls. This one is responsible only for serializing the start,
    /// which in scad is `[`.
    ///
    /// The length of the sequence may or may not be known ahead of time. This
    /// doesn't make a difference in scad because the length is not represented
    /// explicitly in the serialized form. Some serializers may only be able to
    /// support sequences for which the length is known up front.
    ///
    /// The rest of the sequence is serialized by the `ser::SerializeSeq`
    /// implementation for `Serializer` which comes after this impl block.
    fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq> {
        self.output += "[";
        Ok(self)
    }

    /// Tuples are considered sequences in scad.
    fn serialize_tuple(self, len: usize) -> Result<Self::SerializeTuple> {
        self.serialize_seq(Some(len))
    }

    /// All non-sequences, like structs and enums (and units)
    /// are serialized as `name(...);`.
    fn serialize_tuple_struct(
        self,
        name: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeTupleStruct> {
        self.output += name;
        self.output += "(";
        Ok(self)
    }

    /// Tuple variants are serialized as tuple structs with the name of the
    /// enum. The variant's name is ignored.
    fn serialize_tuple_variant(
        self,
        name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
        len: usize,
    ) -> Result<Self::SerializeTupleVariant> {
        self.serialize_tuple_struct(name, len)
    }

    /// Maps are represented as key=value pairs. It makes no sense
    /// for a map to be a value in a struct. In that case use
    /// `#[serde(flatten)]` for the field with the map.
    fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap> {
        if self.output.ends_with('=') {
            return Err(ser::Error::custom("Cannot serialized a map as a value"));
        };
        Ok(self)
    }

    /// All non-sequences, like structs and enums (and units)
    /// are serialized as `name(...);`.
    fn serialize_struct(self, name: &'static str, _len: usize) -> Result<Self::SerializeStruct> {
        self.output += name;
        self.output += "(";
        Ok(self)
    }

    /// Struct variants are represented in scad as a struct with the name of
    /// the enum. The variant's name is ignored.
    fn serialize_struct_variant(
        self,
        name: &'static str,
        _variant_index: u32,
        _variant: &'static str,
        len: usize,
    ) -> Result<Self::SerializeStructVariant> {
        self.serialize_struct(name, len)
    }
}

/// `SerializeSeq` serializes sequence elements and ends the sequence
/// with `]`. `serialize_seq` on the `Serializer` is responsible for
/// writing the starting `[`.
impl<'a> ser::SerializeSeq for &'a mut Serializer {
    // Must match the `Ok` type of the serializer.
    type Ok = ();
    // Must match the `Error` type of the serializer.
    type Error = Error;

    // Serialize a single element of the sequence.
    fn serialize_element<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        if !self.output.ends_with('[') {
            self.output += ",";
        }
        // TODO: create SeqSerializer which limits the types which can be serialized in seqs
        value.serialize(&mut **self)
    }

    // Close the sequence.
    fn end(self) -> Result<()> {
        self.output += "]";
        Ok(())
    }
}

/// `SerializeTuple` is identical to `SerializeSeq`. Tuples are
/// treated as sequences in `serde_scad`.
impl<'a> ser::SerializeTuple for &'a mut Serializer {
    type Ok = ();
    type Error = Error;

    fn serialize_element<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        if !self.output.ends_with('[') {
            self.output += ",";
        }
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<()> {
        self.output += "]";
        Ok(())
    }
}

/// Tuple structs however are treated like structs with "positional
/// arguments" like in `cube(1,true)`. `SerializeTupleStruct`
/// serializes all the fields of the tuple separated by commas. `serialize_tuple_struct` is responsible for writing the name and starting `(`.
impl<'a> ser::SerializeTupleStruct for &'a mut Serializer {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        if !self.output.ends_with('(') {
            self.output += ",";
        }
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<()> {
        self.output += ")";
        Ok(())
    }
}

/// `SerializeTupleVariant` is identical to `SerializeTupleStruct`.
impl<'a> ser::SerializeTupleVariant for &'a mut Serializer {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        if !self.output.ends_with('(') {
            self.output += ",";
        }
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<()> {
        self.output += ")";
        Ok(())
    }
}

/// `SerializeMap` gives `key="value"` argument serialization. Not to
/// be confused with `SerializeStruct`.
impl<'a> ser::SerializeMap for &'a mut Serializer {
    type Ok = ();
    type Error = Error;

    /// `MapKeySerializer` is used to restrict what types are allowed
    /// in the key of a map. The key must implement `Display` as it
    /// needs to be formatted into the output without surrounding
    /// quotes.
    fn serialize_key<T>(&mut self, key: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        if !self.output.ends_with('(') {
            self.output += ",";
        }
        self.output += &key.serialize(MapKeySerializer)?;
        Ok(())
    }

    /// Writes `=` and serializes the value of the map. Currently
    /// allow serialization of all types as values.
    fn serialize_value<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        // TODO: MapValueSerializer?
        self.output += "=";
        value.serialize(&mut **self)
    }

    /// The end of a map is not responsible for adding the `)` at the
    /// end as maps don't serialize into scad objects.
    fn end(self) -> Result<()> {
        Ok(())
    }
}

/// `SerializeStruct` serializes the keys and values of the struct
/// after the `serialize_struct` implementation on `Serializer` writes
/// the name of the struct and a starting `(`.
impl<'a> ser::SerializeStruct for &'a mut Serializer {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, key: &'static str, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        if !self.output.ends_with('(') {
            self.output += ",";
        }
        self.output += key;
        self.output += "=";
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<()> {
        self.output += ")";
        Ok(())
    }
}

/// `SerializeStructVariant` is identical to `SerializeStruct`.
impl<'a> ser::SerializeStructVariant for &'a mut Serializer {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, key: &'static str, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        if !self.output.ends_with('(') {
            self.output += ",";
        }
        self.output += key;
        self.output += "=";
        value.serialize(&mut **self)
    }

    fn end(self) -> Result<()> {
        self.output += ")";
        Ok(())
    }
}

////////////////////////////////////////////////////////////////////////////

#[cfg(test)]
mod test {
    use crate::ser::to_string;
    use serde::Serialize;
    use std::borrow::Cow;

    #[test]
    fn primitives() {
        assert_eq!(to_string(&10_i32).unwrap(), "10");
        assert_eq!(to_string(&10.5_f64).unwrap(), "10.5");
        assert_eq!(to_string(&'a').unwrap(), r#""a""#);
        assert_eq!(to_string(&"str").unwrap(), r#""str""#);
    }

    #[test]
    fn bytes() {
        #[derive(Serialize)]
        struct Cube<'a> {
            size: &'a [u8],
        }

        let cube = Cube {
            size: "ABC".as_bytes(),
        };
        assert_eq!(to_string(&cube).unwrap(), "Cube(size=[65,66,67])");
    }

    #[test]
    fn options() {
        #[derive(Serialize)]
        #[serde(rename = "cube")]
        struct Cube {
            size: [f64; 3],
            center: Option<bool>,
        }

        #[derive(Serialize)]
        struct Cube2 {
            size: Option<f64>,
        }

        #[derive(Serialize)]
        struct Cube3(Option<f64>);

        #[derive(Serialize)]
        enum Cube4 {
            Unnamed(Option<f64>),
            Named { size: Option<f64> },
        }

        let cube = Cube {
            size: [1.0, 2.0, 3.0],
            center: None,
        };
        assert_eq!(to_string(&cube).unwrap(), "cube(size=[1,2,3])");

        let cube = Cube {
            size: [1.0, 2.0, 3.0],
            center: Some(false),
        };
        assert_eq!(to_string(&cube).unwrap(), "cube(size=[1,2,3],center=false)");

        let cube = Cube2 { size: None };
        assert_eq!(to_string(&cube).unwrap(), "Cube2()");

        let cube = Cube3(None);
        assert_eq!(to_string(&cube).unwrap(), "Cube3()");

        let cube = Cube4::Unnamed(None);
        assert_eq!(to_string(&cube).unwrap(), "Cube4()");

        let cube = Cube4::Unnamed(Some(1.0));
        assert_eq!(to_string(&cube).unwrap(), "Cube4(1)");

        let cube = Cube4::Named { size: None };
        assert_eq!(to_string(&cube).unwrap(), "Cube4()");

        let cube = Cube4::Named { size: Some(2.5) };
        assert_eq!(to_string(&cube).unwrap(), "Cube4(size=2.5)");
    }

    #[test]
    fn units() {
        #[derive(Serialize)]
        struct Undef {
            value: (),
        }

        #[derive(Serialize)]
        struct Cube;

        #[derive(Serialize)]
        enum Scad {
            TupleVariant(),
            UnitVariant,
        }

        let undef = Undef { value: () };
        assert_eq!(to_string(&undef).unwrap(), "Undef(value=undef)");

        assert_eq!(to_string(&Cube).unwrap(), "Cube()");

        assert_eq!(to_string(&Scad::TupleVariant()).unwrap(), "Scad()");

        assert!(format!("{}", to_string(&Scad::UnitVariant).unwrap_err())
            .contains("Unit variants are disabled"));
    }

    #[test]
    fn seqs() {
        use std::collections::{BinaryHeap, HashSet};

        assert_eq!(to_string(&[1_u32, 2_u32, 3_u32]).unwrap(), "[1,2,3]");

        let mut hs = HashSet::new();
        hs.insert(1);
        hs.insert(1);

        assert_eq!(to_string(&hs).unwrap(), "[1]");

        let mut bh = BinaryHeap::with_capacity(3);
        // heap structure (indexes):
        //   0
        //  / \
        // 1   2
        // heap data ([] is an empty slot):
        //    []
        //   /  \
        // []    []
        // memory: [] [] []
        bh.push(-3);
        //    -3
        //   /  \
        // []    []
        // memory: -3 [] []
        bh.push(5);
        //   -3
        //  /  \
        // 5    []
        // the heap property is violated is 5 should be higher than -3 in a max-heap
        // therefore they must be swapped
        //    5
        //   / \
        // -3   []
        // memory: 5 -3 []
        bh.push(8);
        //   -3
        //  /  \
        // 5    8
        // the heap property is violated as 8 should be higher than 5 in a max-heap
        // therefore they must be swapped
        //    8
        //   / \
        // -3   5
        // memory: 8 -3 5
        assert_eq!(to_string(&bh).unwrap(), "[8,-3,5]");
    }

    #[test]
    fn tuples() {
        assert_eq!(
            to_string(&("str", 1_i32, 2.5_f64)).unwrap(),
            r#"["str",1,2.5]"#
        );
    }

    #[test]
    fn tuple_structs() {
        #[derive(Serialize)]
        #[serde(rename = "test")]
        struct Test(i32, i32);

        #[derive(Serialize)]
        #[serde(rename = "cube")]
        struct Cube<'a>(Cow<'a, [f32]>);

        #[allow(non_upper_case_globals)]
        static cube: Cube = Cube(Cow::Borrowed(&[1.0, 2.0, 3.0]));

        assert_eq!(to_string(&Test(1, 2)).unwrap(), "test(1,2)");
        assert_eq!(to_string(&cube).unwrap(), "cube([1,2,3])");
    }

    #[test]
    fn structs() {
        #[derive(Serialize)]
        #[serde(rename = "cube")]
        struct Cube {
            size: Vec<f32>,
            center: bool,
        }

        let test = Cube {
            size: vec![1.0, 2.0, 1.0],
            center: true,
        };
        assert_eq!(to_string(&test).unwrap(), "cube(size=[1,2,1],center=true)");
    }

    #[test]
    fn enums() {
        #[derive(Serialize)]
        #[serde(rename = "cube")]
        enum Cube<'a> {
            SizeFloat {
                size: f64,
                center: bool,
            },
            SizeSlice {
                size: Cow<'a, [f64; 3]>,
                center: bool,
            },
            Variant(f64, bool),
        }

        let cube = Cube::SizeFloat {
            size: 2.5,
            center: true,
        };
        assert_eq!(to_string(&cube).unwrap(), "cube(size=2.5,center=true)");

        let cube = Cube::SizeSlice {
            size: Cow::Borrowed(&[1.0, 3.0, 2.0]),
            center: true,
        };
        assert_eq!(to_string(&cube).unwrap(), "cube(size=[1,3,2],center=true)");

        let cube = Cube::Variant(1.0, false);
        assert_eq!(to_string(&cube).unwrap(), "cube(1,false)");
    }

    #[test]
    fn maps() {
        use std::collections::{BTreeMap, HashMap};

        macro_rules! collection {
            // map-like
            ($($k:expr => $v:expr),* $(,)?) => {{
                use std::iter::{Iterator, IntoIterator};
                Iterator::collect(IntoIterator::into_iter([$(($k, $v),)*]))
            }};
            // set-like
            ($($v:expr),* $(,)?) => {{
                use std::iter::{Iterator, IntoIterator};
                Iterator::collect(IntoIterator::into_iter([$($v,)*]))
            }};
        }

        #[derive(Serialize)]
        #[serde(untagged)]
        enum Param {
            Vector(Vec<f64>),
            Scalar(f64),
        }
        #[derive(Serialize)]
        enum Cube<'a> {
            HM {
                #[serde(flatten)]
                hashmap: HashMap<&'a str, Option<Param>>,
            },
            HM2(HashMap<&'a str, HashMap<i32, i32>>),
            Btm(BTreeMap<&'a str, Option<bool>>),
        }

        let cube = Cube::HM {
            hashmap: collection!["size" => None],
        };
        // as hashmaps have undefined order we are testing with single keys only
        assert_eq!(to_string(&cube).unwrap(), "Cube()");

        let cube = Cube::HM {
            hashmap: collection!["size" => Some(Param::Vector(vec![1.0,2.0,3.0]))],
        };
        assert_eq!(to_string(&cube).unwrap(), "Cube(size=[1,2,3])");

        let cube = Cube::HM {
            hashmap: collection!["size" => Some(Param::Scalar(5.5))],
        };
        assert_eq!(to_string(&cube).unwrap(), "Cube(size=5.5)");

        let cube = Cube::HM2(collection!["size" => collection![1 => 1]]);
        assert!(format!("{}", to_string(&cube).unwrap_err()).contains("Cannot serialize"));

        let cube = Cube::Btm(collection!["center" => None]);
        assert_eq!(to_string(&cube).unwrap(), "Cube()");

        let cube = Cube::Btm(collection!["center" => Some(false), "option" => Some(true)]);
        assert_eq!(to_string(&cube).unwrap(), "Cube(center=false,option=true)");
    }
}
