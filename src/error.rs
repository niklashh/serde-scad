//! Provides an `Error` type specific to Scad

use std;
use std::fmt::{self, Display};

use serde::{de, ser};

/// This type represents all possible errors that can occur when
/// serializing Scad.
#[derive(Clone, Debug, PartialEq)]
pub enum Error {
    /// Custom message containing a string
    Message(String),
}

/// Alias for a `Result` with the error type `serde_scad::Error`.
pub type Result<T> = std::result::Result<T, Error>;

impl ser::Error for Error {
    fn custom<T: Display>(msg: T) -> Self {
        Error::Message(msg.to_string())
    }
}

impl de::Error for Error {
    fn custom<T: Display>(msg: T) -> Self {
        Error::Message(msg.to_string())
    }
}

impl Display for Error {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::Message(msg) => formatter.write_str(msg),
        }
    }
}

impl std::error::Error for Error {}
